unclutter-xfixes (1.6-2) UNRELEASED; urgency=medium

  * d/control:
    - Fix the Vcs-* URLs.
    - Bump Standards-Version to 4.6.2, no changes needed.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

 -- Stefan Kangas <stefankangas@gmail.com>  Fri, 27 Jan 2023 11:03:56 +0200

unclutter-xfixes (1.6-1) unstable; urgency=medium

  * New maintainer. (Closes: #1017881)
  * New upstream release.
  * Enable hardened build flags.
  * Bump debhelper from old 11 to 13.
  * debian/control:
    - Set debhelper-compat version in Build-Depends.
    - Bump standards version to 4.6.1, no changes needed.
    - Set Rules-Requires-Root: no.
  * Add debian/watch file, using github.
  * debian/copyright: Use standard, machine-readable format.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Name,
    Repository, Repository-Browse.

 -- Stefan Kangas <stefankangas@gmail.com>  Thu, 20 Oct 2022 14:52:11 +0200

unclutter-xfixes (1.5-3) unstable; urgency=low

  * Upload to unstable.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 30 Oct 2019 14:12:55 -0700

unclutter-xfixes (1.5-2) experimental; urgency=medium

  * Conflict with unclutter (<< 8-22~), not with (<< 8-22), to permit
    backporting to buster.
    Thanks to Axel Beckert for noticing this.

 -- Sean Whitton <spwhitton@spwhitton.name>  Wed, 18 Sep 2019 15:59:28 -0700

unclutter-xfixes (1.5-1) experimental; urgency=medium

  * Initial release (Closes: #825809).

 -- Sean Whitton <spwhitton@spwhitton.name>  Tue, 17 Sep 2019 21:37:13 -0700
